import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

var subscriptionKey = "7232581536fb4153a2a5d4a7f397be08";

const config = {
  name: 'translate',
  //connector: 'rest',
  baseURL: 'https://api.cognitive.microsofttranslator.com',
  crud: false,
  options: {
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      'Ocp-Apim-Subscription-Key': subscriptionKey,
    },
  },
  operations: [
    {
      template: {
        method: 'POST',
        url: 'https://api.cognitive.microsofttranslator.com',
      },
      functions: {
        translations: ['api-version','to'],
      },
    },
  ],
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class TranslateDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'translate';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.translate', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
