import {Model, model, property} from '@loopback/repository';
import {Translate} from './translate.model';

@model()
export class TranslateResponse extends Model {
  @property({
    type: 'any',
  })
  translations?: Translate[];

  constructor(data?: Partial<TranslateResponse>) {
    super(data);
  }
}

export interface TranslateResponseRelations {
  // describe navigational properties here
}

export type TranslateResponseWithRelations = TranslateResponse &
  TranslateResponseRelations;
