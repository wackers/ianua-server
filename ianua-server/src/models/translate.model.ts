import {Model, model, property} from '@loopback/repository';

@model()
export class Translate extends Model {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
  })
  text: string;

  @property({
    type: 'string',
    required: true,
  })
  to: string;

  constructor(data?: Partial<Translate>) {
    super(data);
  }
}

export interface TranslateRelations {
  // describe navigational properties here
}

export type TranslateWithRelations = Translate & TranslateRelations;
