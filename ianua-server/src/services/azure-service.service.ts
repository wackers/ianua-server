import {inject, Provider} from '@loopback/core';
import {getService} from '@loopback/service-proxy';
import {AzureRestDataSource} from '../datasources';
import {TranslateResponse} from '../models';

export interface AzureService {
  // this is where you define the Node.js methods that will be
  // mapped to REST/SOAP/gRPC operations as stated in the datasource
  // json file.
  translate(to: string, text: string): Promise<TranslateResponse>;
}

export class AzureServiceProvider implements Provider<AzureService> {
  constructor(
    // AzureRest must match the name property in the datasource json file
    @inject('datasources.AzureRest')
    protected dataSource: AzureRestDataSource = new AzureRestDataSource(),
  ) {}

  value(): Promise<AzureService> {
    return getService(this.dataSource);
  }
}
