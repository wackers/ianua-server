// Uncomment these imports to begin using these cool features!

// import {inject} from '@loopback/core';
import {inject} from '@loopback/core';
import {getModelSchemaRef, post, requestBody} from '@loopback/rest';
import {TranslateRequest, TranslateResponse} from '../models';
import {AzureService} from '../services';

// var http = require('https');
// var options = {
//   host: 'https://api.cognitive.microsofttranslator.com',
//   path: '/',
//   method: 'POST',
// };

// var body = [
//   {
//     detectLanguage: {
//       language: 'vi',
//     },
//     tanslations: [
//       {
//         text: 'Hello',
//         to: 'en',
//       },
//     ],
//   },
// ];

// const translateResponse: ResponseObject = {
//   description: 'translate Response',
//   baseURL: endpoint,
//   url: endpoint,
//   method: 'POST',
//   content: {
//     'application/json': {
//       schema: {
//         type: 'object',
//         title: 'Translate Response',
//         parameters: {
//           'api-version': '3.0',
//           from: 'en',
//           to: ['de', 'it'],
//         },
//         properties: {
//           url: {type: 'string'},
//           to: {type: 'string'},
//           from: {type: 'string'},
//           body: {type: 'string'},
//           parameters: {type: 'string'},
//           title: {type: 'string'},

//           headers: {
//             'Content-Type': {type: 'application/json'},
//             'Ocp-Apim-Subscription-Key': subscriptionKey,
//             properties: {
//               'Content-Type': {type: 'application/json'},
//               'Ocp-Apim-Subscription-Key': {type: 'string'},
//             },
//           },
//         },

//         data: [
//           {
//             text: 'Hello World!',
//           },
//         ],

//         additionalProperties: true,
//         responseType: 'json',
//       },
//     },
//   },
// };

export class TranslateController {
  constructor(
    @inject('services.AzureService')
    protected azureService: AzureService,
  ) {}

  // constructor(@inject(RestBindings.Http.REQUEST) private req: Request) {}

  // Map to `GET /ping`
  @post('/translate', {
    responses: {
      '200': {
        description: 'Translate Text',
        content: {
          'application/json': {schema: getModelSchemaRef(TranslateResponse)},
        },
      },
    },
  })
  translate(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TranslateRequest),
        },
      },
    })
    translateRequest: TranslateRequest,
  ): Promise<TranslateResponse> {
    return this.azureService.translate(
      translateRequest.to,
      translateRequest.text,
    );
  }
}
